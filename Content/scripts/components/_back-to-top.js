var BackToTopComponent = (function BackToTopComponent($) {
	var $component = $("body").find("#back-to-top");

	function attachEvents() {
		$(window).resize(function () {
			reachFooter();
		});
	}

	function reachFooter() {
		$(window).scroll(function () {
			if ($(this).scrollTop() >= 100) {
				$component.addClass("visible");
			} else {
				$component.removeClass("visible");
			}
		});
		$component.click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 500);
		});
	}

	function init() {
		if (!$component.length) return;
		attachEvents();
		reachFooter();
	}

	return {
		init: init
	};
}(jQuery));
BackToTopComponent.init();