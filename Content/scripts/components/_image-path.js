var imagePath = (function imagePath($) {
    var domain = "";
    var elements = [
        {
            "img": {
                "attr": "src"
            }
        },
        {
            "[srcset]": {
                "attr": "srcset"
            }
        },
        {
            "[data-srcset]": {
                "attr": "data-srcset"
            }
        },
        {
            "[data-src]": {
                "attr": "data-src"
            }
        },
        {
            "[data-bg]": {
                "attr": "data-bg"
            }
        }
    ];
    var strings = ["globalassets", "contentassets"];

    function replacePaths() {
        for (index in elements) {
            var obj = elements[index];
            for (el in obj) {
                if ($(el).length > 0) {
                    var data = obj[el];
                    $(el).each(function () {
                        for (i in strings) {
                            var absPath = domain + strings[i];
                            var path = $(this).attr(data.attr).split(strings[i]);
                            if (typeof path[1] != "undefined") {
                                var newPath = absPath + path[1];
                                $(this).attr(data.attr, newPath);
                            }
                        }
                    });
                }
            }
        }
    }

    return {
        init: function (url, selectors, stringsToSplit) {
            if (typeof url != "undefined") {
                domain = url;

                if (typeof selectors != "undefined") {
                    if (selectors != null) {
                        elements = selectors;
                    }
                }

                if (typeof stringsToSplit != "undefined") {
                    if (stringsToSplit != null) {
                        strings = stringsToSplit;
                    }
                }

                replacePaths();

                $("body").on('DOMSubtreeModified', function () {
                    replacePaths();
                });
            }
        }
    };
}(jQuery));