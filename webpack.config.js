var path = require("path");
var webpack = require("webpack");
var fs = require("fs");
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var appBasePath = "./Features/";
var jsEntries = {};
var cssEntries = {};

var walk = function (dir) {
    var results = [];
    var list = fs.readdirSync(dir);
    list.forEach(function (file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            /* Recurse into a subdirectory */
            var mainjs = file + "/presentation/scripts/main.js";
            var mainjsMobile = file + "/presentation/scripts/main.mobile.js";
            if (fs.existsSync(mainjs)) {
                results.push({ name: path.basename(file), path: mainjs });
            }

            if (fs.existsSync(mainjsMobile)) {
                results.push({ name: path.basename(file) + ".mobile", path: mainjsMobile });
            }
            results = results.concat(walk(file));
        }
    });
    return results;
}

var walkSass = function (dir) {
    var results = [];
    var list = fs.readdirSync(dir);
    list.forEach(function (file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if (stat && !stat.isDirectory()) {
            results.push({ name: path.basename(file).replace(".scss", ""), path: file });
        }
    });
    return results;
}

var cssResults = walkSass("./content/sass/");
cssResults.forEach(result => cssEntries[result.name] = result.path.replace("//", "/"));
console.log(cssResults);

results = walk(appBasePath);
results.forEach(result => jsEntries[result.name] = result.path.replace("//", "/"));
console.log(results);


var config = {
    module: {},
    resolve: {
        alias: {
            'vue$': "vue/dist/vue.esm.js"
        },
        extensions: ["*", ".js", ".vue", ".json"],
        modules: [
            path.resolve("./features"),
            path.resolve("./node_modules")
        ]
    },
    performance: {
        hints: false
    },
    devtool: "#eval-source-map"
}

if (process.env.NODE_ENV === "production") {
    config.devtool = "#source-map";
    config.plugins = (config.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ]);
}


var scriptConfig = Object.assign({},
    config,
    {
        entry: jsEntries,
        output: {
            path: path.resolve(__dirname, "./content/scripts/app"),
            publicPath: "/content/scripts/app",
            filename: "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        "vue-style-loader",
                        "css-loader"
                    ]
                }, {
                    test: /\.vue$/,
                    loader: "vue-loader",
                    options: {
                        loaders: {
                            js: {
                                loader: "babel-loader"
                            }
                        }
                    }
                },
                {
                    test: /\.js$/,
                    loader: "babel-loader",
                    exclude: /node_modules/
                },
                {
                    test: /\.(png|jpg|gif|svg)$/,
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]?[hash]"
                    }
                }
            ]
        }
    });

const extractPlugin = new ExtractTextPlugin('[name].css');
var cssConfig = Object.assign({},
    config,
    {
        entry: cssEntries,
        output: {
            path: path.resolve(__dirname, "./content/css"),
            publicPath: "/content/css",
            filename: "[name].css"
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    include: [path.resolve(__dirname, 'content', 'sass')],
                    use: extractPlugin.extract({
                        use: [
                            { loader: 'css-loader', options: { url: false, minimize: process.env.NODE_ENV === "production", sourceMap: process.env.NODE_ENV === "production" } },
                            { loader: 'sass-loader' }
                        ],
                    })
                }
            ]
        },
        plugins: [
            extractPlugin
        ],
    }
);

module.exports = [cssConfig];
